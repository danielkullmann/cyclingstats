#!/usr/bin/env python3

import argparse
import json
import os
import sys

from scrapy.crawler import CrawlerProcess
from cyclingstats.spiders.procyclingstats import ProcyclingstatsSpider

BASE_DIR = os.path.join("..", "cyclingstats-data")

parser = argparse.ArgumentParser(description='Crawl racing data from procyclingtats.com.')
parser.add_argument('race', type=str, help='the race to crawl (e.g. tour-de-france)')
parser.add_argument('year', type=int, help='the year of the race (e.g. 2020)')
parser.add_argument(
    '--force', '-f', dest='force', action='store_true', default=False,
    help='Even crawl when the data fle exists already')

args = parser.parse_args()

race = args.race
year = args.year
force = args.force

filename = os.path.join(BASE_DIR, race + "-" + str(year) + ".csv")

print("#", race, year, "=>", filename)

if force or not os.path.exists(filename):

    process = CrawlerProcess(settings={
        "CONCURRENT_REQUESTS": 2,
        "TELNETCONSOLE_ENABLED": False,
        "LOG_LEVEL": "WARN",
        "FEEDS": {
            filename: {"format": "csv"},
        }
    })
    process.crawl(ProcyclingstatsSpider, race=race, year=year)

    process.start()

metadata = {
    "races": []
}

metafile = os.path.join(BASE_DIR, "meta.json")

if os.path.exists(metafile):
    with open(metafile, "r") as fh:
        metadata = json.load(fh)

found = False
for (mrace, myears) in metadata["races"]:
    if mrace == race:
        found = True
        if year not in myears:
            myears.append(year)
            myears.sort()
            break

if not found:
    metadata["races"].append((race, [year]))

with open(metafile, "w") as fh:
    json.dump(metadata, fh, indent=2)
