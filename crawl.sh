#!/bin/sh

for year in $(seq 2000 2021); do
    python3 ./crawl.py dauphine "$year"
done

for year in $(seq 1996 2020); do
    python3 ./crawl.py tour-de-france "$year"
done
python3 ./crawl.py -f tour-de-france 2021

for year in $(seq 2000 2021); do
    python3 ./crawl.py giro-d-italia "$year"
done

for year in $(seq 2000 2020); do
    python3 ./crawl.py vuelta-a-espana "$year"
done

for year in $(seq 2000 2021); do
    python3 ./crawl.py paris-nice "$year"
done

for year in $(seq 2000 2021); do
    python3 ./crawl.py milano-sanremo "$year"
done
