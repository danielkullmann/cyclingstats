/* globals Chart */
"use strict";

var data = {};
var meta = null;
var oRace = null;
var oChart = null;

var mPlotOptions = {
    maintainAspectRatio: false,
    legend: {
        onClick: removeRiderFromLegend
    },
    /* In case I want to display information about a stage..
    onClick: function(e) {
        var xLabel = this.scales['x-axis-0'].getValueForPixel(e.x);
        console.log(xLabel);
    },
    */
    plugins:  {
        zoom: {
            zoom: {
                enabled: true,
                drag: true,
                mode: "x"
            }
        },
        colorschemes: {
            scheme: 'brewer.SetOne9'
        }
    }
};

/*
    First use case: Compare GC standings of riders over a certain race
    * DONE Select race from drop-down
    * DONE load race data
    * IN_PROGRESS convert it to useful structure
      * Group data by stage and ranking
    * TODO For the last stage, get the first 10 riders on the GC
*/

function getItem(header, row, key) {
    var index = header.indexOf(key);
    if (index < 0) {
        throw key + " not in " + header;
    }
    return row[index];
}

function findRow(rows, header, key, value) {
    for (var row of rows) {
        if (value === getItem(header, row, key)) {
            return row;
        }
    }
    return null;
}

function getListOfRankings(race) {
    var rankings = new Set();
    for (var ranking of race.overall) {
        rankings.add(ranking);
    }

    rankings = Array.from(rankings);
    rankings.sort();
    return rankings;
}

function getListOfRiders(race) {
    var riders = new Set();
    var row;
    if (race.stages) {
        for (var stage of race.stages) {
            if (stage.GC) {
                for (row of stage.GC) {
                    var rider = getItem(race.header, row, "Rider");
                    riders.add(rider);
                }
            }
        }
    }
    if (race.overall.GC) {
        for (row of race.overall.GC) {
            rider = getItem(race.header, row, "Rider");
            riders.add(rider);
        }
    }
    riders = Array.from(riders);
    riders.sort();
    return riders;
}

function createChart(stages, datasets) {
    var ctx = document.getElementById("plot1");
    if (oChart && oChart.destroy) {
        oChart.destroy();
    }

    oChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: stages,
            datasets: datasets
        },
        options: mPlotOptions
    });

    ctx.addEventListener("dblclick", function() {
        oChart.resetZoom();
    });


}

function removeRider(sRider) {
    // delete rider from chart
    var oIndex = 0;
    for (var dataset of oChart.data.datasets) {
        if (dataset.label.endsWith(" " + sRider)) {
            console.log(oIndex);
            console.log(dataset);
            oChart.data.labels.splice(oIndex, 1);
            oChart.data.datasets.splice(oIndex, 1);
            oChart.update();
            break;
        }
        oIndex += 1;
    }
}

function removeRiderFromLegend(_oEvent, oItem) {
    var sRider = oItem.text.split(" ")[1];
    removeRider(sRider);
}

function selectRace(oEvent) {
    var oSelectRace = document.getElementById("race");
    var oSelectYear = document.getElementById("year");

    var race = oSelectRace.value;
    var year = oSelectYear.value;
    if (race == "" || year == "") return;

    loadRaceData(race, year).then(data => {
        var header = data.header;
        var gc_overall = data.overall.GC;
        var ten = Math.min(10, gc_overall.length);
        var topRiders = gc_overall.slice(0, ten).map(aItem => {
            return getItem(header, aItem, "Rider");
        });
        if (data.stages) {
            var stages = Object.keys(data.stages).map(sIndex => parseInt(sIndex)+1);
            var i = 1;
            var chartData = topRiders.map(riderName => {
                var riderRankings = data.stages.map(stage => {
                    if (stage.GC) {
                        var row = findRow(stage.GC, header, "Rider", riderName);
                        if (row) {
                            return parseInt(getItem(header, row, "Rnk"));
                        } else {
                            return null;
                        }
                    } else {
                        return null;
                    }
                });
                riderName = "" + i + " " + riderName;
                i += 1;
                return {
                    label: riderName,
                    data: riderRankings,
                    fill: false,
                }
            });
            oRace = data;
            createChart(stages, chartData);
            // var aRiders = getListOfRiders(data);
        } else {
            // This is a one-day race
        }
    });
}

function compareByRnk(header, d1, d2) {
    var rank1 = parseInt(getItem(header, d1, "Rnk"));
    var rank2 = parseInt(getItem(header, d2, "Rnk"));
    if (rank1 < rank2) {
        return -1;
      }
      if (rank1 > rank2) {
        return 1;
      }
      // a must be equal to b
      return 0;
    }

function parseCSV(str) {
    var lines = str.split("\n");
    var header = null;
    var content = {};
    lines.forEach(line => {
        var columns = line.split(",");
        if (header === null) {
            header = columns;
        } else {
            var stage = getItem(header, columns, "stage");
            var ranking = getItem(header, columns, "ranking");
            if (stage === "") { return; }

            if (stage == "overall") {
                if (!(stage in content)) { content[stage] = {}; }
                if (!(ranking in content[stage])) { content[stage][ranking] = []; }
                content[stage][ranking].push(columns);
            } else if (stage.startsWith("stage-")) {
                if (!("stages" in content)) { content["stages"] = []; }
                stage = parseInt(stage.slice(6))-1;
                if (content["stages"][stage] == null) { content["stages"][stage] = {}; }
                if (!(ranking in content["stages"][stage])) { content["stages"][stage][ranking] = []; }
                content["stages"][stage][ranking].push(columns);
            } else {
                throw "unknown stage: " + stage;
            }
        }
    });
    if (content.overall) {
        for (var data of Object.values(content.overall)) {
            data.sort(compareByRnk.bind(null, header));
        }
    }
    if (content.stages) {
        for (const stage of content.stages) {
            if (stage === undefined) continue;
            for (data of Object.values(stage)) {
                data.sort(compareByRnk.bind(null, header));
            }
        }
    }
    if (!content.overall) {
        content.overall = content.stages[content.stages.length-1];
    }
    content.header = header;
    return content;
}

function loadRaceData(race, year) {
    if (race in data && year in data[race]) {
        return Promise.resolve(data[race][year]);
    }
    if (!(race in data) ) {
        data[race] = {};
    }
    var url = "../cyclingstats-data/" + race + "-" + year + ".csv?t="+new Date().getTime();
    return fetch(url).then(oResponse => {
        return oResponse.text().then(sText => {
            data[race][year] = parseCSV(sText);
            data[race][year].race = race;
            data[race][year].year = year;
            return data[race][year];
        });
    });
}

function fillRaceSelector() {
    var oSelect = document.getElementById("race");
    oSelect.onchange = fillYearSelector;

    var oElement = document.createElement("option");
    oElement.value = "";
    oElement.append("--select a race--");
    oSelect.append(oElement);

    meta.races.forEach(oItem => {
        var race = oItem[0];
        var oElement = document.createElement("option");
        oElement.value = race;
        oElement.append(race);
        oSelect.append(oElement);
    });
}

function fillYearSelector() {
    var oSelectRace = document.getElementById("race");
    var oSelect = document.getElementById("year");
    oSelect.onchange = selectRace;

    // Remove existing options
    while (oSelect.options.length) {
        oSelect.remove(0);
    }

    var oElement = document.createElement("option");
    oElement.value = "";
    oElement.append("--select a year--");
    oSelect.append(oElement);

    meta.races.forEach(oItem => {
        var race = oItem[0];
        if (race == oSelectRace.value) {
            var years = oItem[1];
            years.sort().reverse();
            years.forEach(year => {
                var oElement = document.createElement("option");
                oElement.value = year;
                oElement.append(year);
                oSelect.append(oElement);
            });
            }
    });
}

window.onload = function() {
    fetch("../cyclingstats-data/meta.json?t="+new Date().getTime()).then(oResponse => {
        oResponse.json().then(oResponse => {
            meta = oResponse;
            fillRaceSelector();
        });
    });
}
