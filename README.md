# Scrape Cycling Statistics from procyclingstats.com

This project contains a Scrapy spider to get race data from https://procyclingstats.com.

The goal is to create special kinds of graphs, e.g. for these questions:

* How do certain riders progress in a multi-day stage race?
* How do certain riders compare over multiple races?

The webapp is automatically deployed on https://danielkullmann.gitlab.io/cyclingstats/.

## How to Use the Spider

First, you need to have scrapy installed:

```bash
pip install scrapy
```

Then, when you are in the root directory, you can issue commands like the following,
replacing race and year options with what you want (`-o` is for output file, in this 
case as a CSV file):

```bash
scrapy crawl pcs_race -a race=dauphine -a year=2020 -o dauphine-2019.csv
```

There is also a script, `crawl.py`, that can be used to scrape a predefined set of races.

## Planned features

* Scrape more races
* Other type of graphs (which ones?)
