import scrapy

from functools import partial
from urllib.parse import urlparse

class ProcyclingstatsSpider(scrapy.Spider):
    name = 'pcs_race'
    allowed_domains = ['www.procyclingstats.com']

    def __init__(self, race=None, year=None, *args, **kwargs):
        super(ProcyclingstatsSpider, self).__init__(*args, **kwargs)
        self.start_urls = ['https://www.procyclingstats.com/race/' +str(race) + "/" + str(year)]

    def parse(self, response):
        (race, year) = urlparse(response.url).path.split("/")[2:]
        for element in response.css("div.pageSelectNav:nth-child(2) form select option"):
            url = element.attrib["value"]
            pieces = url.split("/")[1:]
            #if len(pieces) != 4:
            #    raise Exception("Error: wrong number of pieces: " + str(pieces))
            (race, year, stage) = pieces[0:3]
            if stage.startswith("stage-"):
                next_page = 'https://www.procyclingstats.com/race/' + url
                yield scrapy.Request(next_page, callback=partial(self.parse_result, race, year, stage))
        yield from self.parse_result(race, year, "overall", response)

    def parse_result(self, race, year, stage, response):
        rankings = response.css("div.left ul.restabs li")
        tables = response.css('div.left table.results')
        for (ranking_selector, table) in zip(rankings, tables):
            ranking = ranking_selector.css("a::text").get() or "Stage"
            if ranking == "Stage" and not stage.startswith("stage-"):
                continue
            yield from self.parse_sub_result(race, year, stage, ranking, table)
        else:
            if len(rankings) == 0 and len(tables) == 1:
                yield from self.parse_sub_result(race, year, stage, "GC", tables[0])

    def parse_sub_result(self, race, year, stage, ranking, table):
        rows = table.css("tr")
        header_columns = None
        for row in rows:
            if not header_columns:
                header_columns = [self.convert_header_column(col) for col in row.css("th")]
            columns = [self.convert_data_column(col) for col in row.css("td")]
            if len(columns) == 0: continue
            data = {
                "stage": stage,
                "ranking": ranking
            }
            data.update(dict(zip(header_columns, columns)))
            yield data

    def convert_header_column(self, col):
        return "".join([s.strip() for s in col.css("::text").getall()])

    def convert_data_column(self, col):
        if col.css("a"):
            anchor = col.css("a")
            href_split = anchor.attrib["href"].split("/")
            if len(href_split) == 2:
                return href_split[1]
            return anchor.css("::text").get()
        items = [s.strip() for s in col.css("::text").getall()]
        if len(items) == 2 and (items[0] == items[1] or items[0] == ",,"):
            return items[1]
        return "".join(items)
